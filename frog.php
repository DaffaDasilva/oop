<?php

    require_once "animal.php";

    class Frog extends Animal{
        public $legs4 = 4;

        public function jump(){
            echo $this->get_name()."<br/>";
            echo $this->legs4."<br/>";
            echo "hop hop<br/>";
        }
    }

?>
