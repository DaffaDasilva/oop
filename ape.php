<?php
    require_once "animal.php";

    class Ape extends Animal{
        public function yell(){
            echo $this->get_name()."<br/>";
            echo $this->get_legs()."<br/>";
            echo "Auooo<br/>";
        }
    }
?>
