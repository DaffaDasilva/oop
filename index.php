<?php

require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("shaun");

echo $sheep->get_name(); // "shaun"
echo "<br/>";
echo $sheep->legs; // 2
echo "<br/>";
echo $sheep->cold_blooded; // false
echo "<br/>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "<br/>";
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"

echo "<br/>";
$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

?>
